export interface ITags {
    id: string;
    name: string;
    color: string;
    bg: string;
}

export interface ITask {
    id: string;
    completed: boolean;
    title: string;
    description: string;
    deadline: string;
    tag: ITags;
}

export interface ICreateTask {
    title: string;
    description: string;
    deadline: string;
    tag: string;
}

export interface IUpdateTask extends ICreateTask {
    completed: boolean;
}
