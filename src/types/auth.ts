export interface ISignUp {
    email: string;
    name: string;
    password: string;
    confirmPassword?: string;
}

export interface ILogin extends Omit<ISignUp, 'name' | 'confirmPassword'> {}

export interface IResponseDataToken {
    data: string;
}
