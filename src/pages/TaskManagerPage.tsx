import { TasksControl } from '../components/TasksControl';

export const TaskManagerPage: React.FC = () => {
    return (
        <main>
            <TasksControl />
        </main>
    );
};
