import { SignUp } from '../components/forms/SignUp';

export const SignUpPage: React.FC = () => {
    return (
        <main>
            <section className = 'publish-tip sign-form'>
                <SignUp />
            </section>
        </main>
    );
};
