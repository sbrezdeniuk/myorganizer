import { Login } from '../components/forms/Login';

export const LoginPage: React.FC = () => {
    return (
        <main>
            <section className = 'sign-form'>
                <Login />
            </section>
        </main>
    );
};
