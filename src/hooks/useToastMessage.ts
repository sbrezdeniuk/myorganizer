import { useEffect } from 'react';
import { toast } from 'react-toastify';
import { toastOptions } from '../constants/toastOptions';
import { getMessage } from '../lib/redux/selectors/ui';
import { uiAction } from '../lib/redux/actions/ui';
import { useAppSelector, useAppDispatch } from '../lib/redux/init/store';


export const useToastMessage = () => {
    const dispatch = useAppDispatch();
    const { errorMessage, successMessage, infoMessage } = useAppSelector(getMessage);

    useEffect(() => {
        if (errorMessage) {
            toast.error(errorMessage, toastOptions);
            dispatch(uiAction.resetError());
        }
    }, [errorMessage]);

    useEffect(() => {
        if (successMessage) {
            toast.success(successMessage, toastOptions);
            dispatch(uiAction.resetSuccess());
        }
    }, [successMessage]);

    useEffect(() => {
        if (infoMessage) {
            toast.info(infoMessage, toastOptions);
            dispatch(uiAction.resetInfo());
        }
    }, [infoMessage]);
};
