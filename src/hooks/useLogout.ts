
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { getAuthState } from '../lib/redux/selectors';
import { actionAuth } from '../lib/redux/actions/auth';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

export const useLogout = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [isLogout, setIsLogout] = useState<boolean>(false);
    const { token } = useAppSelector(getAuthState);

    const logoutFetch = () => {
        dispatch(actionAuth.fetchLogoutAsync());
        setIsLogout(true);
    };

    useEffect(() => {
        if (isLogout && !token) {
            navigate('/login');
        }
    }, [token]);

    return { logoutFetch };
};
