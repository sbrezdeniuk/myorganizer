import { useAppDispatch } from '../lib/redux/init/store';
import { ICreateTask } from '../types';
import { taskAction } from '../lib/redux/actions';

export const useCreateTask = () => {
    const dispatch = useAppDispatch();

    const newTaskFetch = (credentials: ICreateTask) => {
        dispatch(taskAction.createTaskAsync(credentials));
    };

    return { newTaskFetch  };
};
