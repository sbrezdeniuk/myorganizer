import { useAppDispatch } from '../lib/redux/init/store';
import { taskAction } from '../lib/redux/actions/tasks';

export const useTags = () => {
    const dispatch = useAppDispatch();
    const fetchTags = async () => {
        await dispatch(taskAction.fetchTagsAsync());
    };

    return { fetchTags };
};
