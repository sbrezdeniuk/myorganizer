import { useAppDispatch } from '../lib/redux/init/store';
import { ICreateTask, ITask, IUpdateTask } from '../types';
import { taskAction } from '../lib/redux/actions';

export const useTasks = () => {
    const dispatch = useAppDispatch();

    const newTaskFetch = (credentials: ICreateTask) => {
        dispatch(taskAction.createTaskAsync(credentials));
    };

    const fetchTasks = () => {
        dispatch(taskAction.fetchTasksAsync());
    };

    const selectedTask = (task: ITask) => {
        dispatch(taskAction.setSelectedTask(task));
    };

    const fetchUpdateTask = (id: string, task: IUpdateTask) => {
        dispatch(taskAction.editTaskAsync(id, task));
    };

    const fetchDeleteTask = (id: string) => {
        dispatch(taskAction.deleteTaskAsync(id));
    };

    return {
        newTaskFetch,
        fetchTasks,
        selectedTask,
        fetchUpdateTask,
        fetchDeleteTask,
    };
};
