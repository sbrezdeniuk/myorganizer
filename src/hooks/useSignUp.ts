
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { getAuthState } from '../lib/redux/selectors';
import { ISignUp } from '../types';
import { actionAuth } from '../lib/redux/actions/auth';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

export const useSignUp = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [isSignUp, setIsSignUp] = useState<boolean>(false);
    const { token, isFetching } = useAppSelector(getAuthState);

    const fetchSignUp = (credentials: ISignUp) => {
        dispatch(actionAuth.fetchSignUpAsync(credentials));
        setIsSignUp(true);
    };

    useEffect(() => {
        if (isSignUp && token) {
            navigate('/task-manager');
        }
    }, [token]);

    return { isFetching, fetchSignUp };
};
