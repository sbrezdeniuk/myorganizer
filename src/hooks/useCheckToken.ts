
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

export const useCheckToken = () => {
    const navigate = useNavigate();
    const localToken = localStorage.getItem('token');

    useEffect(() => {
        if (!localToken) {
            navigate('/login');
        } else {
            navigate('/task-manager');
        }
    }, [localToken]);
};
