
import { useState, useEffect } from 'react';
// import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getAuthState } from '../lib/redux/selectors';
import { ILogin } from '../types';
import { actionAuth } from '../lib/redux/actions/auth';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

export const useLogin = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [isLogin, setIsLogin] = useState<boolean>(false);
    const { token, isFetching } = useAppSelector(getAuthState);

    const loginFetch = (credentials: ILogin) => {
        dispatch(actionAuth.fetchLoginAsync(credentials));
        setIsLogin(true);
    };

    useEffect(() => {
        if (isLogin && token) {
            navigate('/task-manager');
        }
    }, [token]);

    return { isFetching, loginFetch };
};
