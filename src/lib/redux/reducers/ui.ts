import { AnyAction } from 'redux';
import { uiTypes } from '../types';

export const initialStateUI = {
    errorMessage:   '',
    successMessage: '',
    infoMessage:    '',
    error:          false,
};

export const uiReducer = (state = initialStateUI, action: AnyAction) => {
    switch (action.type) {
        case uiTypes.RESET_ERROR: {
            return {
                ...state,
                errorMessage: '',
                error:        false,
            };
        }
        case uiTypes.RESET_SUCCESS_MESSAGE: {
            return {
                ...state,
                successMessage: '',
            };
        }
        case uiTypes.RESET_INFO_MESSAGE: {
            return {
                ...state,
                infoMessage: '',
            };
        }
        case uiTypes.SET_ERROR_MESSAGE: {
            return {
                ...state,
                error:        true,
                errorMessage: action.payload,
            };
        }
        case uiTypes.SET_SUCCESS_MESSAGE: {
            return {
                ...state,
                successMessage: action.payload,
            };
        }
        case uiTypes.SET_INFO_MESSAGE: {
            return {
                ...state,
                infoMessage: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};
