import { AnyAction } from 'redux';
import { tasksTypes } from '../types';

export const initialStateTasks = {
    tags:        [],
    tasks:       [],
    slectedTask: null,
    isFetching:  false,
};

export const tasksReducer = (state = initialStateTasks, action: AnyAction) => {
    switch (action.type)   {
        case tasksTypes.SET_TAGS: {
            return {
                ...state,
                tags: action.payload,
            };
        }
        case tasksTypes.START_FETCHING_TASKS: {
            return {
                ...state,
                isFetching: true,
            };
        }
        case tasksTypes.STOP_FETCHING_TASKS: {
            return {
                ...state,
                isFetching: false,
            };
        }
        case tasksTypes.SET_TASKS: {
            return {
                ...state,
                tasks: action.payload,
            };
        }
        case tasksTypes.SET_SELECTED_TASK: {
            return {
                ...state,
                slectedTask: action.payload,
            };
        }
        case tasksTypes.RESET_SELECTED_TASK: {
            return {
                ...state,
                slectedTask: null,
            };
        }
        default: {
            return state;
        }
    }
};
