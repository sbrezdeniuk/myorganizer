import { uiTypes } from '../types';

export const uiAction = Object.freeze({
    resetError: () => {
        return {
            type: uiTypes.RESET_ERROR,
        };
    },
    setError: (message: string) => {
        return {
            type:    uiTypes.SET_ERROR_MESSAGE,
            error:   true,
            payload: message,
        };
    },
    setSuccess: (message: string) => {
        return {
            type:    uiTypes.SET_SUCCESS_MESSAGE,
            payload: message,
        };
    },
    resetSuccess: () => {
        return {
            type: uiTypes.RESET_SUCCESS_MESSAGE,
        };
    },
    setInfo: (message: string) => {
        return {
            type:    uiTypes.SET_INFO_MESSAGE,
            payload: message,
        };
    },
    resetInfo: () => {
        return {
            type: uiTypes.RESET_INFO_MESSAGE,
        };
    },
});
