import { api } from '../../../api';
import {
    ITags, ICreateTask, ITask, IUpdateTask,
} from '../../../types';
import { AppThunk } from '../init/store';
import { uiAction } from './ui';
import { tasksTypes } from '../types';

export const taskAction = Object.freeze({
    setTags: (tags: ITags[]) => {
        return {
            type:    tasksTypes.SET_TAGS,
            payload: tags,
        };
    },
    startFetching: () => {
        return {
            type: tasksTypes.START_FETCHING_TASKS,
        };
    },
    stopFetching: () => {
        return {
            type: tasksTypes.STOP_FETCHING_TASKS,
        };
    },
    setTasks: (tasks: ITask[]) => {
        return {
            type:    tasksTypes.SET_TASKS,
            payload: tasks,
        };
    },
    setSelectedTask: (task: ITask) => {
        return {
            type:    tasksTypes.SET_SELECTED_TASK,
            payload: task,
        };
    },
    resetSelectedTask: () => {
        return {
            type: tasksTypes.RESET_SELECTED_TASK,
        };
    },
    fetchTagsAsync: (): AppThunk => async (dispatch) => {
        try {
            dispatch(taskAction.startFetching());
            const tags = await api.fetchTags();
            dispatch(taskAction.setTags(tags));
        } catch (error) {
            const { message } = error as Error;
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(taskAction.stopFetching());
        }
    },
    fetchTasksAsync: (): AppThunk => async (dispatch) => {
        try {
            dispatch(taskAction.startFetching());
            const task = await api.fetchTasks();
            dispatch(taskAction.setTasks(task));
        } catch (error) {
            const { message } = error as Error;
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(taskAction.stopFetching());
        }
    },
    createTaskAsync: (credentials: ICreateTask): AppThunk => async (dispatch) => {
        try {
            dispatch(taskAction.startFetching());
            await api.createTask(credentials);
            dispatch(taskAction.fetchTasksAsync());
            dispatch(uiAction.setInfo('Задача добавлена!'));
        } catch (error) {
            const { message } = error as Error;
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(taskAction.stopFetching());
        }
    },
    editTaskAsync: (id: string, credentials: IUpdateTask): AppThunk => async (dispatch) => {
        try {
            dispatch(taskAction.startFetching());
            await api.editTask(id, credentials);
            dispatch(uiAction.setInfo(`Задача с идентификатором ${id} успешно обновлена.`));
            dispatch(taskAction.fetchTasksAsync());
        } catch (error) {
            const { message } = error as Error;
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(taskAction.resetSelectedTask());
            dispatch(taskAction.stopFetching());
        }
    },
    deleteTaskAsync: (id: string): AppThunk => async (dispatch) => {
        try {
            dispatch(taskAction.startFetching());
            await api.deleteTask(id);
            dispatch(uiAction.setInfo('Задача удалина'));
            dispatch(taskAction.fetchTasksAsync());
        } catch (error) {
            const { message } = error as Error;
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(taskAction.resetSelectedTask());
            dispatch(taskAction.stopFetching());
        }
    },
});
