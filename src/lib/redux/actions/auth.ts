import { api } from '../../../api';
import { ILogin, ISignUp } from '../../../types';
import { AppThunk } from '../init/store';
import { uiAction } from './ui';
import { authTypes } from '../types/auth';

export const actionAuth = Object.freeze({
    setToken: (token: string) => {
        return {
            type:    authTypes.SET_TOKEN,
            payload: token,
        };
    },
    startFetching: () => {
        return {
            type: authTypes.START_FETCHING,
        };
    },
    stopFetching: () => {
        return {
            type: authTypes.STOP_FETCHING,
        };
    },
    fetchLoginAsync: (credentials: ILogin): AppThunk => async (dispatch) => {
        try {
            dispatch(actionAuth.startFetching());
            const newToken = await api.login(credentials);
            localStorage.setItem('token', newToken.data);
            dispatch(actionAuth.setToken(newToken.data));
            dispatch(uiAction.setSuccess('Добро пожаловать!'));
        } catch (error) {
            const { message } = error as Error;
            localStorage.removeItem('token');
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(actionAuth.stopFetching());
        }
    },
    fetchLogoutAsync: ():AppThunk => async (dispatch) => {
        try {
            dispatch(actionAuth.startFetching());
            await api.logout();
            localStorage.removeItem('token');
            dispatch(actionAuth.setToken(''));
            dispatch(uiAction.setInfo('Возвращайтесь поскорее ;) Мы будем скучать.'));
        } catch (error) {
            const { message } = error as Error;
            localStorage.removeItem('token');
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(actionAuth.stopFetching());
        }
    },
    fetchSignUpAsync: (credentials: ISignUp): AppThunk => async (dispatch) => {
        try {
            dispatch(actionAuth.startFetching());
            const newToken = await api.signup(credentials);
            localStorage.setItem('token', newToken.data);
            dispatch(actionAuth.setToken(newToken.data));
            dispatch(uiAction.setSuccess(`Добро пожаловать! ${credentials.name}`));
        } catch (error) {
            const { message } = error as Error;
            localStorage.removeItem('token');
            dispatch(uiAction.setError(message));
        } finally {
            dispatch(actionAuth.stopFetching());
        }
    },
});
