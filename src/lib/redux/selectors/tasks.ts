import { RootState } from '../init/store';
import { ITags, ITask } from '../../../types';

export const getTags = (state: RootState): ITags[] => {
    return state.tasksReducer.tags;
};

export const getTasks = (state: RootState): ITask[] => {
    return state.tasksReducer.tasks;
};

export const getSelectedTask = (state: RootState): ITask => {
    return state.tasksReducer.slectedTask;
};

