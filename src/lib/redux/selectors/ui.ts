import { RootState } from '../init/store';
import { initialStateUI } from '../reducers/ui';

export const getErrorMessage = (state: RootState): string => {
    return state.uiReducer.errorMessage;
};

export const getSuccessMessage = (state: RootState): string => {
    return state.uiReducer.successMessage;
};

export const getInfoMessage = (state: RootState): string => {
    return state.uiReducer.infoMessage;
};

export const getMessage = (state: RootState): typeof initialStateUI => {
    return state.uiReducer;
};
