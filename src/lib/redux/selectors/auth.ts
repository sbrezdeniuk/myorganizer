import { RootState } from '../init/store';
import { initialState } from '../reducers/auth';

export const getToken = (state: RootState): string => {
    return state.authReducer.token;
};

export const getAuthState = (state: RootState): typeof initialState => {
    return state.authReducer;
};
