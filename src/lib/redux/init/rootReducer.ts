// Core
import { combineReducers } from 'redux';

// Reducers
import { authReducer, uiReducer, tasksReducer } from '../reducers';


export const rootReducer = combineReducers({
    authReducer,
    uiReducer,
    tasksReducer,
});
