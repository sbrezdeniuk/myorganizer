export const authTypes = Object.freeze({
    SET_TOKEN:       'SET_TOKEN',
    START_FETCHING:  'START_FETCHING',
    STOP_FETCHING:   'STOP_FETCHING',
    UPDATE_PASSWORD: 'UPDATE_PASSWORD',
});
