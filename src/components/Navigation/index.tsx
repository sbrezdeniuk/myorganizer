import { Link, NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getToken } from '../../lib/redux/selectors/auth';
import { useLogout } from '../../hooks';

export const Navigation: React.FC = () => {
    const token = useSelector(getToken);
    const { logoutFetch } = useLogout();

    return (
        <nav>
            { !token && <Link to = '/login' className = 'active'>Войти</Link> }
            { token && (
                <>
                    <NavLink
                        to = '/task-manager'
                        activeClassName = 'active' >
                            К задачам
                    </NavLink>
                    <NavLink
                        to = '/profile'
                        activeClassName = 'active'>
                            Профиль
                    </NavLink>
                    <button className = 'button-logout' onClick = { logoutFetch }>Выйти</button>
                </>
            ) }
        </nav>
    );
};
