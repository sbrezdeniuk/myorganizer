import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

import { ILogin } from '../../../types';
import { Input } from '../elements';
import { schema } from './config';
import { useLogin } from '../../../hooks/useLogin';

export const Login: React.FC = () => {
    const { isFetching, loginFetch } = useLogin();
    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (credentials: ILogin) => {
        await loginFetch(credentials);
        form.reset();
    });

    return (
        <form onSubmit = { onSubmit }>
            <fieldset disabled = { isFetching }>
                <legend>Вход</legend>
                <Input
                    role = 'email'
                    placeholder = 'Электропочта'
                    error = { form.formState.errors.email }
                    register = { form.register('email') } />
                <Input
                    role = 'password'
                    placeholder = 'Пароль'
                    type = 'password'
                    error = { form.formState.errors.password }
                    register = { form.register('password') } />
                <button type = 'submit' className = 'button-login'>Войти</button>
            </fieldset>
            <p>
                Если у вас до сих пор нет учётной записи, вы можете <Link to = '/signup'>зарегистрироваться</Link>.
            </p>
        </form>
    );
};
