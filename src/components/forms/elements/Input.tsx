import { UseFormRegisterReturn } from 'react-hook-form';

export const Input: React.FC<IPropTypes> = (props) => {
    let input = (
        <input
            placeholder = { props.placeholder }
            type = { props.type }
            role = { props.role }
            min = { props.minDate }
            className = { props.customClass ? props.customClass : '' }
            { ...props.register } />
    );

    if (props.tag === 'textarea') {
        input = (
            <textarea
                placeholder = { props.placeholder }
                className = { props.customClass ? props.customClass : '' }
                { ...props.register }></textarea>
        );
    }

    return (
        <label className = 'label'>
            { props.label }
            <div>
                <span className = 'errorMessage'>{ props.error?.message }</span>
            </div>
            { input }
        </label>
    );
};

Input.defaultProps = {
    type: 'text',
    tag:  'input',
};

interface IPropTypes {
    placeholder?: string;
    type?: string;
    tag?: string;
    label?: string;
    customClass?: string;
    role?: string;
    minDate?: string;
    register: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
    options?: {
        value: string;
        name: string;
    }
}

