import * as yup from 'yup';
import { ICreateTask } from '../../../types';


// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'минимальная длина - ${min} символов';
// eslint-disable-next-line no-template-curly-in-string
const tooLongMessage = 'максимальная длина - ${max} символов';

export const schema: yup.SchemaOf<ICreateTask> = yup.object().shape({
    title:       yup.string().min(3, tooShortMessage).max(40, tooLongMessage).required('*'),
    deadline:    yup.string().required('*'),
    description: yup.string().min(3, tooShortMessage).max(250, tooLongMessage).required('*'),
    tag:         yup.string().required('*'),
});
