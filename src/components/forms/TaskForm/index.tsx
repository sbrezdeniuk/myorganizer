import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTags, useTasks } from '../../../hooks';

import { Input } from '../elements';
import { ICreateTask } from '../../../types';
import { schema } from './config';
import { useAppSelector } from '../../../lib/redux/init/store';
import { getTags, getSelectedTask } from '../../../lib/redux/selectors';
import { Tags } from './tags';

const InputDeadline = styled.input`
    & div#picker: {
        display: none !important;
    }
`;


export const TaskForm: React.FC<{ closeForm: () => void }> = ({ closeForm }) => {
    const { fetchTags } = useTags();
    const { newTaskFetch, fetchUpdateTask, fetchDeleteTask } = useTasks();
    const tags = useAppSelector(getTags);
    const selectedTask = useAppSelector(getSelectedTask);

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    useEffect(() => {
        if (tags.length === 0) {
            void fetchTags();
        }
    }, [tags]);

    useEffect(() => {
        if (selectedTask) {
            form.setValue('title', selectedTask.title);
            form.setValue('deadline', new Date(selectedTask.deadline).toISOString().slice(0, 10));
            form.setValue('description', selectedTask.description);
            form.setValue('tag', selectedTask.tag.id);
        }
    }, [selectedTask]);

    const onSubmit = form.handleSubmit((task: ICreateTask) => {
        if (selectedTask) {
            fetchUpdateTask(selectedTask.id, { ...task, completed: selectedTask.completed });
        } else {
            newTaskFetch(task);
        }
        form.reset();
        closeForm();
    });

    const handleUpdateTask = () => {
        const {
            title, deadline, description, tag,
        } = form.getValues();
        fetchUpdateTask(
            selectedTask.id,
            {
                completed: true,
                title,
                deadline,
                description,
                tag,
            },
        );
        form.reset();
        closeForm();
    };

    const handelDeleteTask = () => {
        fetchDeleteTask(selectedTask.id);
        form.reset();
        closeForm();
    };

    return (
        <form onSubmit = { onSubmit }>
            <div className = 'head'>
                { selectedTask && (
                    <>
                        <button className = 'button-complete-task' onClick = { handleUpdateTask }>завершить</button>
                        <div className = 'button-remove-task' onClick = { handelDeleteTask }></div>
                    </>
                )
                }
            </div>
            <div className = 'content'>
                <Input
                    role = 'title'
                    label = 'Задачи'
                    customClass = 'title'
                    placeholder = 'Пройти интенсив по React + Redux + TS + Mobx'
                    register = { form.register('title') } />
                <div className = 'deadline'>
                    <span className = 'label'>Дедлайн</span>
                    <InputDeadline
                        type = 'date'
                        min = { new Date().toISOString().slice(0, 10) }
                        { ...form.register('deadline', { value: new Date().toISOString().slice(0, 10) }) } />
                </div>
                <div className = 'description'>
                    <Input
                        tag = 'textarea'
                        label = 'Описание'
                        customClass = 'text'
                        placeholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.'
                        register = { form.register('description') } />
                </div>
                <Tags
                    tags = { tags }
                    setValue = { (id) => form.setValue('tag', id) }
                    selectedTagId = { selectedTask?.tag?.id } />
                <div className = 'errors'>
                    <p className = 'errorMessage'>
                        {
                            form.formState.errors.description?.message
                            || form.formState.errors.title?.message
                            || form.formState.errors.deadline?.message
                        }
                    </p>
                </div>
                <div className = 'form-controls'>
                    <button
                        type = 'reset'
                        disabled = { !form.formState.isDirty && !form.formState.isValid }
                        className = 'button-reset-task'>
                            Reset
                    </button>
                    <button
                        type = 'submit'
                        className = 'button-save-task'
                        disabled = { !form.formState.isDirty && !form.formState.isValid }>
                            Save
                    </button>
                </div>
            </div>
        </form>
    );
};
