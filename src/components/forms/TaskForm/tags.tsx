import { useState, useEffect } from 'react';
import { ITags } from '../../../types';

export const Tags: React.FC<{
    tags: ITags[]; selectedTagId?: string; setValue: (id: string) => void;
}> = ({ tags, selectedTagId, setValue }) => {
    const [selectedTag, setSelectedTag] = useState<string>('');

    useEffect(() => {
        if (tags.length !== 0 && !selectedTagId) {
            setSelectedTag(tags[ 0 ].id);
            setValue(tags[ 0 ].id);
        }
        if (selectedTagId) {
            setSelectedTag(selectedTagId);
            setValue(selectedTagId);
        }
    }, [tags, selectedTagId]);

    const handleClick = (id: string) => {
        setSelectedTag(id);
        setValue(id);
    };

    return (
        <div className = 'tags'>
            { tags.map((tag: ITags) => (
                <span
                    key = { tag.id }
                    onClick = { () => handleClick(tag.id) }
                    className = { selectedTag === tag.id ? 'tag selected' : 'tag' }
                    style = { { color: tag.color, backgroundColor: tag.bg } }>
                    { tag.name }
                </span>
            ))
            }
        </div>
    );
};
