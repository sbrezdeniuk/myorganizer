import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

import { ISignUp } from '../../../types/auth';

import { Input } from '../elements';
import { schema } from './config';
import { useSignUp } from '../../../hooks';

export const SignUp: React.FC = () => {
    const { isFetching, fetchSignUp } = useSignUp();
    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (credentials: ISignUp) => {
        const { confirmPassword, ...newUser } = credentials;
        await fetchSignUp(newUser);
        form.reset();
    });

    return (
        <form onSubmit = { onSubmit }>
            <fieldset disabled = { isFetching }>
                <legend>Регистрация</legend>
                <Input
                    role = 'name'
                    placeholder = 'Имя и фамилия'
                    error = { form.formState.errors.name }
                    register = { form.register('name') } />
                <Input
                    role = 'email'
                    placeholder = 'Электропочта'
                    error = { form.formState.errors.email }
                    register = { form.register('email') } />
                <Input
                    role = 'password'
                    placeholder = 'Пароль'
                    type = 'password'
                    error = { form.formState.errors.password }
                    register = { form.register('password') } />
                <Input
                    role = 'confirmPassword'
                    placeholder = 'Подтверждение пароля'
                    type = 'password'
                    error = { form.formState.errors.confirmPassword }
                    register = { form.register('confirmPassword') } />
                <button type = 'submit' className = 'button-login'>Зарегистрироваться</button>
            </fieldset>
            <p>Перейти к <Link to = '/login'>логину</Link>.</p>
        </form>
    );
};
