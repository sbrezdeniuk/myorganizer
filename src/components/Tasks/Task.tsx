import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { ITask } from '../../types';
import { useTasks } from '../../hooks';

export const Task: React.FC<{ task: ITask, showForm: () => void }> = ({ task, showForm }) => {
    const {
        title, deadline, tag, completed,
    } = task;
    const { selectedTask } = useTasks();

    const handelChangeTasks = () => {
        selectedTask(task);
        showForm();
    };

    return (
        <div className = { `task ${completed && 'completed'}` } onClick = { handelChangeTasks } >
            <span className = 'title'>{ title }</span>
            <div className = 'meta'>
                <span className = 'deadline'>{ format(new Date(deadline), 'dd MMM yyyy', { locale: ru }) }</span>
                <span className = 'tag' style = { { color: tag.color, backgroundColor: tag.bg } }>{ tag.name }</span>
            </div>
        </div>
    );
};
