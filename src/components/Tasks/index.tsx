import { useEffect } from 'react';
import { useAppSelector } from '../../lib/redux/init/store';
import { getTasks } from '../../lib/redux/selectors';
import { ITask } from '../../types';
import { Task } from './Task';
import { useTasks } from '../../hooks';

export const Tasks: React.FC<{ showForm: () => void }> = (
    { showForm },
) => {
    const { fetchTasks } = useTasks();
    const tasks = useAppSelector(getTasks);

    useEffect(() => {
        fetchTasks();
    }, []);

    return (
        <>
            {
                tasks.length === 0
                    ? <div className = 'list empty' />
                    : <div className = 'list'>
                        <div className = 'tasks'>
                            {
                                tasks.map(
                                    (task: ITask) => <Task
                                        key = { task.id }
                                        task = { task }
                                        showForm = { showForm } />,
                                )
                            }
                        </div>
                    </div>
            }
        </>
    );
};
