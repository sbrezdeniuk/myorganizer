import { useState } from 'react';
import { Tasks } from '../Tasks';
import { TaskForm } from '../forms/TaskForm';

export const TasksControl: React.FC = () => {
    const [isForm, setIsForm] = useState<boolean>(false);

    const showForm = () => {
        setIsForm(true);
    };

    const hiddenForm = () => {
        setIsForm(false);
    };

    return (
        <>
            <div className = 'controls'>
                <i className = 'las' />
                <button className = 'button-create-task' onClick = { showForm }>Новая задача</button>
            </div>
            <div className = 'wrap'>
                <Tasks showForm = { showForm } />
                { isForm ? (
                    <div className = 'task-card'>
                        <TaskForm  closeForm = { hiddenForm } />
                    </div>
                ) : null }
            </div>
        </>
    );
};
