
export const Footer: React.FC = () => {
    return (
        <footer>
            <span>
                © 2021 Lectrum LLC - All Rights Reserved.
            </span>
        </footer>
    );
};
