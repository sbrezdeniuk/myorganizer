// Core
import axios, { AxiosResponse } from 'axios';

import {
    ISignUp, ILogin, IResponseDataToken, ITags, ICreateTask, ITask, IUpdateTask,
} from '../types';


export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';

export const api = Object.freeze({
    getVersion() {
        return '0.0.1';
    },
    get token(): string | null {
        return localStorage.getItem('token');
    },
    async signup(credentials: ISignUp): Promise<IResponseDataToken> {
        const { data } = await axios.post<ISignUp, AxiosResponse<IResponseDataToken>>(`${TODO_API_URL}/auth/registration`,
            credentials,
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

        return data;
    },
    async login(credentials: ILogin): Promise<IResponseDataToken> {
        const { email, password } = credentials;
        const { data } = await axios.get<ILogin, AxiosResponse<IResponseDataToken>>(
            `${TODO_API_URL}/auth/login`,
            {
                headers: {
                    Authorization: `Basic ${window.btoa(`${email}:${password}`)}`,
                },
            },
        );

        return data;
    },
    async logout(): Promise<void> {
        await axios.get(`${TODO_API_URL}/auth/logout`, {
            method:  'GET',
            headers: {
                Authorization: `Bearer ${api.token}`,
            },
        });
    },
    async fetchTags(): Promise<ITags[]> {
        const { data } = await axios.get<ITags[]>(`${TODO_API_URL}/tags`, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        return data;
    },
    async fetchTasks(): Promise<ITask[]> {
        const { data } = await axios.get<AxiosResponse<ITask[]>>(`${TODO_API_URL}/tasks`, {
            headers: {
                Authorization: `Bearer ${api.token}`,
            },
        });

        return data?.data;
    },
    async createTask(task: ICreateTask): Promise<ITask> {
        const { data } = await axios.post<AxiosResponse<ITask>>(`${TODO_API_URL}/tasks`,
            task,
            {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });

        return data?.data;
    },
    async editTask(id: string, task: IUpdateTask): Promise<ITask> {
        const { data } = await axios.put<AxiosResponse<ITask>>(`${TODO_API_URL}/tasks/${id}`,
            task,
            {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });

        return data?.data;
    },
    async deleteTask(taskId: string): Promise<void> {
        await axios.delete<AxiosResponse<ITask>>(`${TODO_API_URL}/tasks/${taskId}`,
            {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });
    },
});
