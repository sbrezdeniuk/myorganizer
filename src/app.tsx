// Core
import { useEffect } from 'react';
import {
    Routes, Route, Outlet, Navigate,
} from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';

// Components
import {
    LoginPage, SignUpPage, TaskManagerPage, ProfilePage,
} from './pages';
import { Navigation } from './components/Navigation';
import { Footer } from './components/Footer';
import { useToastMessage, useCheckToken } from './hooks';
import { useAppSelector, useAppDispatch } from './lib/redux/init/store';
import { getToken } from './lib/redux/selectors';
import { actionAuth } from './lib/redux/actions/auth';
// Instruments


export const App: React.FC = () => {
    useCheckToken();
    useToastMessage();
    const tokenLocal = localStorage.getItem('token');
    const dispatch = useAppDispatch();
    const token = useAppSelector(getToken);

    useEffect(() => {
        if (!token && tokenLocal) {
            dispatch(actionAuth.setToken(tokenLocal));
        }
    }, [token]);

    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />
            <Navigation />
            <Routes>
                <Route path = '/' element = { <Outlet /> }>
                    <Route path = '' element = { <TaskManagerPage /> } />
                    <Route path = 'task-manager' element = { <TaskManagerPage /> } />
                    <Route path = 'profile' element = { <ProfilePage /> } />
                </Route>
                <Route path = '/login' element = { <LoginPage /> } />
                <Route path = '/signup' element = { <SignUpPage /> } />
                <Route path = '*' element = { <Navigate to = '/' replace /> } />
            </Routes>
            <Footer />
        </>
    );
};

